const userData = 'https://reqres.in/api/users';
// const userData = 'https://reqres.in/api/users?page=1';


const getUsers = async(page) => {
    try {
        const resp = await fetch(`${userData}?page=${page}`);
        if (!resp.ok) throw 'Error en la peticion';

        const { data: users } = await resp.json();
        // console.log(users);

        return users

    } catch (error) {
        throw error
    }
}





export {
    getUsers
}